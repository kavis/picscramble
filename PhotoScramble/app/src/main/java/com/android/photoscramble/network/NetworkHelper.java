package com.android.photoscramble.network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Kavis on 8/5/2016.
 */

///
// Helper class to do networking operations
///
public class NetworkHelper
{
    private static NetworkHelper sSingleton;
    private RequestQueue        mRequestQueue;
    private Context             mContext;

    // constructor.
    private NetworkHelper(Context context){
        mContext = context;
        mRequestQueue = getRequestQueue();
    }


    ///
    // get helper.
    ///
    public static synchronized NetworkHelper sSingleton(Context context){
        if(sSingleton == null){
            sSingleton = new NetworkHelper(context);
        }
        return sSingleton;
    }

    ///
    // Get volley request queue
    ///
    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }


    ///
    // Add requests to request queue.
    ///
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}