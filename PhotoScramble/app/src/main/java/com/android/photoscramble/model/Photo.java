package com.android.photoscramble.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kavis on 8/5/2016.
 */

////
// Simple model class to hold data of flickr photo.
///
public class Photo {

    private final String    mTitle;
    private final String    mLink;
    private final String    mImageUrl;
    private boolean         mBackVisible;

    public String getTitle() {
        return mTitle;
    }

    public String getLink() {
        return mLink;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public boolean getBackVisible()
    {
        return mBackVisible;
    }

    public Photo (JSONObject object) throws JSONException {
        mTitle = object.getString("title");
        mLink = object.getString("link");
        mImageUrl = object.getJSONObject("media").getString("m");
    }

    public void setBackVisible(boolean visible)
    {
        mBackVisible = visible;
    }

}
