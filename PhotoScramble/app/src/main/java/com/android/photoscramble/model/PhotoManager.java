package com.android.photoscramble.model;

import android.content.Context;
import android.util.Log;

import com.android.photoscramble.network.NetworkHelper;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kavis on 8/6/2016.
 */


////
// Manager class to which deals with photo
////
public class PhotoManager {

    ///
    // Response handler.
    ///
    public interface PhotoResponseHandler
    {
        public void onResponse(List<Photo> photoList, String error);
    }

    private static PhotoManager sSingleton;
    private static final String URL = "https://api.flickr.com/services/feeds/photos_public.gne?format=json";


    // get manager object.
    public static PhotoManager singleton()
    {
        if (sSingleton == null)
        {
            sSingleton = new PhotoManager();
        }

        return sSingleton;
    }


    ////
    // Fetch photos
    ////
    public void fetchPhotos(Context context, PhotoResponseHandler responseHandler)
    {
        final PhotoResponseHandler _responseHandler = responseHandler;

        StringRequest request = new StringRequest
                (Request.Method.GET, URL, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        String response = s.replace("jsonFlickrFeed(", "");
                        response = response.substring(0, response.length()-1);
                        try {
                            JSONObject object = new JSONObject(response);
                            JSONArray array = object.getJSONArray("items");
                            List<Photo> photos = new ArrayList<>();
                            for (int i=0; i<array.length(); i++)
                            {
                                Photo photo = new Photo(array.getJSONObject(i));
                                photos.add(photo);
                            }
                            _responseHandler.onResponse(photos, null);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("StringRequest", "error " + e.getMessage());
                            _responseHandler.onResponse(null, e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("StringRequest", "error " + volleyError.getMessage());
                        _responseHandler.onResponse(null, volleyError.getMessage());
                    }
                });

        NetworkHelper.sSingleton(context).addToRequestQueue(request);
    }
}
