package com.android.photoscramble.view;

/**
 * Created by Kavis on 8/6/2016.
 */
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.photoscramble.R;
import com.android.photoscramble.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PhotoAdapter  extends RecyclerView.Adapter<PhotoAdapter.ItemViewHolder> {

    // Item Click listener
    public interface  ItemClickListener
    {
        public void onItemClick(int position, View view);
    }

    // Data member
    private List<Photo>             mPhotoList;
    private Context                 mContext;
    private ItemClickListener       mItemClickListener;
    private boolean                 mAnimationEnabled;

    // Constructor
    public PhotoAdapter(Context context, List<Photo> mPhotoList) {
        this.mPhotoList = mPhotoList;
        this.mContext = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item_view, null);
        ItemViewHolder viewHolders = new ItemViewHolder(layoutView);
        return viewHolders;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        final int _pos = position;

        // set click listener for the item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickListener.onItemClick(_pos, v);
            }
        });

        // set Images
        Picasso.with(mContext)
                .load(mPhotoList.get(position).getImageUrl())
                .centerCrop()
                .resize(300, 300)
                .into(holder.mPhotoImageView);

        holder.mPositionText.setText(""+ (position+1));

        if (mAnimationEnabled && mPhotoList.get(_pos).getBackVisible())
        {
            holder.flipCard(false);
        }
        else if (mAnimationEnabled)
        {
            holder.flipCard(true);
        }
    }

    // Change of an item in position
    public void changeItemAtPosition(int position, Photo photo) {
        mPhotoList.set(position, photo);
        notifyItemChanged(position);
    }

    // set photo list
    public void setPhotoList(List<Photo> photoList)
    {
        mPhotoList = photoList;
        mAnimationEnabled = true;
    }

    @Override
    public int getItemCount() {
        return this.mPhotoList.size();
    }


    // Set RecyclerView ItemClickListener
    public void setOnItemClickListener(ItemClickListener clickListener)
    {
        mItemClickListener = clickListener;
    }


    ///
    // View Holder
    ///
    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        public ImageView        mPhotoImageView;
        public View             mCardBackLayout;
        public View             mCardFrontLayout;
        private AnimatorSet     mSetRightOut;
        private AnimatorSet     mSetLeftIn;
        public TextView         mPositionText;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mPhotoImageView = (ImageView)itemView.findViewById(R.id.photo);
            mCardBackLayout = itemView.findViewById(R.id.card_back);
            mCardFrontLayout = itemView.findViewById(R.id.card_front);
            mPositionText = (TextView) itemView.findViewById(R.id.position);
            loadAnimations(itemView.getContext());
        }

        /// Load Animations
        private void loadAnimations(Context context) {
            mSetRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.out_animation);
            mSetLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.in_animation);
        }

        // Flip card animation
        public void flipCard(boolean front) {
            if (!front) {
                mSetRightOut.setTarget(mCardFrontLayout);
                mSetLeftIn.setTarget(mCardBackLayout);
                mSetRightOut.start();
                mSetLeftIn.start();
            } else {
                mSetRightOut.setTarget(mCardBackLayout);
                mSetLeftIn.setTarget(mCardFrontLayout);
                mSetRightOut.start();
                mSetLeftIn.start();
            }
        }

    }


}
