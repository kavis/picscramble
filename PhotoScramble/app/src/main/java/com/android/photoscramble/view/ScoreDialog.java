package com.android.photoscramble.view;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.photoscramble.R;

/**
 * Created by Kavis on 8/6/2016.
 */
public class ScoreDialog extends DialogFragment {

    private static final String SCORE = "score";
    private CallBacks mCallback;

    public interface CallBacks
    {
        public void onPlayAgainClicked();
        public void onExitClicked();
    }

    ///
    // Get instance
    ///
    public static ScoreDialog newInstance(String accuracy) {

        Bundle args = new Bundle();
        args.putString(SCORE, accuracy);
        ScoreDialog fragment = new ScoreDialog();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_NoTitleBar_Fullscreen);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.score_dialog, container);

        TextView textView = (TextView) view.findViewById(R.id.score_text);
        String score = getArguments().getString(SCORE);
        textView.setText("Your Accuracy is " + score+"%");

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (v.getId() == R.id.play_again && mCallback != null)
                {
                    mCallback.onPlayAgainClicked();
                }
                else if (v.getId() == R.id.exit && mCallback != null)
                {
                    mCallback.onExitClicked();
                }

                dismiss();
            }
        };
        view.findViewById(R.id.play_again).setOnClickListener(clickListener);
        view.findViewById(R.id.exit).setOnClickListener(clickListener);
        return view;
    }

    // set callbacks
    public void setCallback(CallBacks callback)
    {
        mCallback = callback;
    }
}
