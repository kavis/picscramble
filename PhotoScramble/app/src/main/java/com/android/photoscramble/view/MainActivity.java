package com.android.photoscramble.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.photoscramble.R;
import com.android.photoscramble.model.Photo;
import com.android.photoscramble.model.PhotoManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    // constants.
    public static final int RIGHT = 100;
    public static final int WRONG = 200;

    // view member
    private TextView                mCounterTextView;
    private RecyclerView            mRecyclerView;
    private ProgressDialog          mProgressDialog;

    // data member
    private List<Photo>             mPhotoList;
    private List<Integer>           mFilledPosition = new ArrayList<>();
    private Photo                   mPhotoPresented;
    private int                     mPositiveGuess;
    private int                     mTotalGuess;
    private Handler                 mAutoDismissTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    ///
    // Create activity.
    ////
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        mCounterTextView = (TextView) findViewById(R.id.counter);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Fetching photos from Flickr");

        if (mPhotoList == null)
        {
            fetchPhotos();
        }
        else
        {
            initializeRecyclerView();
        }
    }


    ///
    // Fetch photos.
    ///
    private void fetchPhotos()
    {
        mProgressDialog.show();
        PhotoManager.singleton().fetchPhotos(this, new PhotoManager.PhotoResponseHandler() {
            @Override
            public void onResponse(List<Photo> photoList, String error) {
                if (error == null) {
                    mProgressDialog.dismiss();

                    // keep only 9 items.
                    if (photoList.size() >= 9) {
                        mPhotoList = new ArrayList<Photo>();
                        for (int i = 0; i < 9; i++) {
                            mPhotoList.add(photoList.get(i));
                        }

                        initializeRecyclerView();

                    } else {
                        mProgressDialog.dismiss();
                        showError("Items are less than 9");
                    }
                } else {
                    mProgressDialog.dismiss();
                    showError(error);
                }
            }
        });
    }


    ///
    // Show error dialog.
    ///
    private void showError(String error)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(error);
        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fetchPhotos();
            }
        });

        builder.show();
    }

    ////
    //Initialize recyclerView.
    ///
    private void initializeRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        final PhotoAdapter adapter = new PhotoAdapter(this, mPhotoList);
        adapter.setOnItemClickListener(new PhotoAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {

                mTotalGuess++;

                if (mPhotoPresented != null) {

                    // If guess is correct.
                    if (mPhotoPresented.getImageUrl().equals(mPhotoList.get(position).getImageUrl())) {

                        showGuess(RIGHT);
                        mPositiveGuess++;
                        Photo photo = mPhotoList.get(position);
                        photo.setBackVisible(false);
                        adapter.changeItemAtPosition(position, photo);
                        mFilledPosition.add(position);
                    } else {
                        showGuess(WRONG);
                    }

                    presentPhotoToUser();
                }
            }
        });

        mCounterTextView.setVisibility(View.VISIBLE);
        new PhotoCountDownTimer().start();

        mRecyclerView.setAdapter(adapter);
    }

    ////
    // Show whether the guess was right or wrong.
    ///
    private void showGuess(int guess)
    {
        ImageView imageView = (ImageView) findViewById(R.id.guess);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(guess == RIGHT ? R.drawable.right : R.drawable.wrong);

        // Ensure that only one timeout is set.
        if (mAutoDismissTimer != null)
        {
            mAutoDismissTimer.removeCallbacksAndMessages(null);
            mAutoDismissTimer = null;
        }

        mAutoDismissTimer = new Handler();
        mAutoDismissTimer.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                // Since this is called asynchronously, ensure that the activity is still alive.
                if (!isFinishing())
                {
                    ImageView imageView = (ImageView) findViewById(R.id.guess);
                    imageView.setVisibility(View.GONE);
                }
            }
        }, 2000);

    }


    ///
    // Present photo to user.
    ///
    private void presentPhotoToUser()
    {
        if (mFilledPosition.size() < 9)
        {
            ImageView photoImageView = (ImageView) findViewById(R.id.photo_option);
            photoImageView.setVisibility(View.VISIBLE);
            mPhotoPresented = getRandomPhoto();
            Picasso.with(this)
                    .load(mPhotoPresented.getImageUrl())
                    .centerCrop()
                    .resize(400, 400)
                    .into(photoImageView);
        }
        else
        {
            showScore();
        }
    }


    ////
    // Show score.
    ///
    private void showScore() {

        ScoreDialog scoreDialog = ScoreDialog.newInstance(""+ ((mPositiveGuess*100) / mTotalGuess));
        scoreDialog.setCallback(new ScoreDialog.CallBacks() {
            @Override
            public void onPlayAgainClicked() {
                fetchPhotos();
                reset();

                ImageView photoImageView = (ImageView) findViewById(R.id.photo_option);
                photoImageView.setVisibility(View.GONE);
            }

            @Override
            public void onExitClicked() {
                finish();
            }
        });

        scoreDialog.show(getFragmentManager(), "SCORE_DIALOG");
    }


    ///
    // reset the game.
    ///
    private void reset()
    {
        mFilledPosition.clear();
        mPhotoList = null;
        mPositiveGuess = 0;
        mTotalGuess = 0;

        if (mRecyclerView != null)
        {
            mRecyclerView.setAdapter(null);
        }
    }


    ///
    // Reset the activity
    ///
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        reset();
    }

    ///
    // Give random photo from the list.
    ///
    private Photo getRandomPhoto() {

        int result = new Random().nextInt(9);

        if (mFilledPosition.contains(result))
        {
            return getRandomPhoto();
        }

        return mPhotoList.get(result);
    }


    ///
    // Countdown timer for user to remember the position of photos.
    ///
    public class PhotoCountDownTimer extends CountDownTimer {

        private static final long START_TIME = 20000;
        private static final long INTERVAL = 1000;

        public PhotoCountDownTimer() {

            super(START_TIME, INTERVAL);

        }

        @Override

        public void onFinish() {

            PhotoAdapter adapter = (PhotoAdapter) mRecyclerView.getAdapter();

            for (int i=0; i<9; i++)
            {
                mPhotoList.get(i).setBackVisible(true);
            }

            adapter.setPhotoList(mPhotoList);
            adapter.notifyDataSetChanged();
            mCounterTextView.setVisibility(View.GONE);
            presentPhotoToUser();

        }

        @Override

        public void onTick(long millisUntilFinished) {

            mCounterTextView.setText("" + (millisUntilFinished/1000));

        }
    }


}
